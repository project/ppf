<?php

namespace Drupal\ppf;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Extension\ThemeExtensionList;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Theme\ThemeManagerInterface;

/**
 * Provides a service for Preprocessor Files.
 */
final class PreprocessorFiles {
  use StringTranslationTrait;

  /**
   * The ID of the main configuration for the module.
   *
   * @var string
   */
  public const CONFIG_ID = 'ppf.settings';

  /**
   * ID of the 'Preprocessor Files Directory' configuration.
   *
   * Allows specification of the directory to load preprocessor files from.
   *
   * e.g. By default, it searches in '/themes/custom/YOUR_THEME/preprocess'
   *
   * @var string
   */
  public const CONFIG__PREPROCESSOR_FILES_DIRECTORY = 'preprocessor_files_directory';

  /**
   * Stores default value of the 'preprocessor files Directory' configuration.
   *
   * @var string
   */
  public const CONFIG__PREPROCESSOR_FILES_DIRECTORY__DEFAULT = 'preprocess';

  /**
   * ID of the 'Preprocessor Files Extension' configuration.
   *
   * Allows specification of the directory to load preprocessor files from.
   *
   * e.g. By default, it searches for '*.preprocessor.php' files.
   *
   * @var string
   */
  public const CONFIG__PREPROCESSOR_FILES_EXTENSION = 'preprocessor_files_extension';

  /**
   * Stores default value of the 'Preprocessor Files Extension' configuration.
   *
   * @var string
   */
  public const CONFIG__PREPROCESSOR_FILES_EXTENSION__DEFAULT = '.preprocessor.php';

  /**
   * Use DI to inject Drupal's configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  public ConfigFactoryInterface $configFactory;

  /**
   * Drupal's Theme Manager injected through DI.
   *
   * @var \Drupal\Core\Theme\ThemeManagerInterface
   */
  public ThemeManagerInterface $themeManager;

  /**
   * Drupal's Theme Extension List injected through DI.
   *
   * @var \Drupal\Core\Extension\ThemeExtensionList
   */
  public ThemeExtensionList $themeExtensionList;

  /**
   * Constructs the PreprocessorFiles service.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Configuration Factory service injected through DI.
   * @param \Drupal\Core\Theme\ThemeManagerInterface $themeManager
   *   Theme Manager service injected through DI.
   * @param \Drupal\Core\Extension\ThemeExtensionList $themeExtensionList
   *   Theme Extension List service injected through DI.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    ThemeManagerInterface $themeManager,
    ThemeExtensionList $themeExtensionList,
  ) {
    $this->configFactory = $configFactory;
    $this->themeManager = $themeManager;
    $this->themeExtensionList = $themeExtensionList;
  }

  /**
   * Shortcut to obtain the PreprocessorFiles service from the global container.
   *
   * @return \Drupal\ppf\PreprocessorFiles
   *   The PreprocessorFiles service.
   */
  public static function service() : PreprocessorFiles {
    static $service;
    if (!empty($service)) {
      return $service;
    }
    $service = \Drupal::service('ppf');
    return $service;
  }

  /**
   * Get main configuration for the Preprocessor Files module.
   *
   * @return \Drupal\Core\Config\ImmutableConfig
   *   The immutable configuration object.
   */
  public function config() : ImmutableConfig {
    static $config = NULL;
    if (!empty($config)) {
      return $config;
    }
    $config = $this->configFactory->get(self::CONFIG_ID);
    return $config;
  }

  /**
   * Get the directory to search for preprocessor files in.
   *
   * This is relative to the currently active theme.
   *
   * @return string
   *   Returns the path to the configured directory in the current active theme.
   */
  public function getPreprocessorFilesDirectory() : string {
    static $directory;
    if ($directory !== NULL) {
      return $directory;
    }
    $theme = $this->themeManager->getActiveTheme()->getName();
    $themePath = $this->themeExtensionList->getPath($theme);
    $directoryName = $this->config()->get(self::CONFIG__PREPROCESSOR_FILES_DIRECTORY) ?? self::CONFIG__PREPROCESSOR_FILES_DIRECTORY__DEFAULT;
    $directory = $themePath . '/' . $directoryName;
    return $directory;
  }

  /**
   * Get the configured preprocessor files extension.
   *
   * @return string
   *   Returns the file extension that is configured.
   */
  public function getPreprocessorFilesExtension() : string {
    static $extension;
    if ($extension !== NULL) {
      return $extension;
    }
    $extension = $this->config()->get(self::CONFIG__PREPROCESSOR_FILES_EXTENSION) ?? self::CONFIG__PREPROCESSOR_FILES_EXTENSION__DEFAULT;
    return $extension;
  }

  /**
   * Check if the configured preprocess directory exists in the active theme.
   *
   * @return bool
   *   Returns TRUE if the directory exists. Returns FALSE otherwise.
   */
  public function preprocessorFilesDirectoryExistsInTheme() : bool {
    static $exists;
    if ($exists !== NULL) {
      return $exists;
    }
    $directory = self::getPreprocessorFilesDirectory();
    $exists = file_exists($directory);
    return $exists;
  }

}
