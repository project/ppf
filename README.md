# Preprocessor Files

The Preprocessor Files module allows you to create dedicated files for
preprocessing templates.

- [Quickstart](#quickstart)
- [Core Features](#core-features)
- [Configuration](#configuration)
- [How It Works](#how-it-works)

## Quickstart

Enable the module as you would any other. Doing so will not immediately change
anything on your site.

Now, you can create **Preprocessor Files**.

Visit `/admin/config/ppf` to customize the module as you see fit. You can also
generate a starter folder in your default
theme.

Create a `YOUR_THEME/preprocess/node.preprocess.php` file with the following
contents.

_If you changed the default configurations, adjust the directory name and file
extension accordingly._

```php
<?php

$variables['foo'] = 'bar';
```

You should now notice that in `node.html.twig`, you have access to the `foo`
variable, which contains `bar`.

Anything in this file is akin to being inside `hook_preprocess_node()`. You can
alter variables as you see fit.

## Core Features

Similar to [Preprocess Functions](https://www.drupal.org/docs/8/theming-drupal-8/modifying-attributes-in-a-theme-file),
**Preprocessor Files** can be used to preprocess variables for any given template.

Normally, for the `node.html.twig` template, you would create a
`hook_preprocess_node();` function in your theme if you wanted to alter
variables before template rendering.

With Preprocessor Files, you can create a dedicated file called
`node.preprocess.php` in your theme. This file will work exactly like a
preprocess function and by default will be housed in
`themes/custom/YOUR_THEME/preprocess`.

Below is an example of the contents this file will have. You can use this
blueprint to create your own.

```php
<?php

/**
 * @file
 * Preprocess override for a template. Acts as a preprocess function.
 *
 * Any modifications done in this file are equivalent to doing them within a
 * hook_preprocess_HOOK() implementation.
 *
 * The modifications here execute after traditional hooks if both are
 * implemented.
 *
 * The loading of this file is provided by the Preprocessor Files module.
 * @see \Drupal\ppf\PreprocessorFiles
 *
 * Available variables:
 * @var array $variables
 *   The variables array.
 *   The same you would get from a traditional preprocess hook.
 * @var string $hook
 *   The theme hook.
 * @var array $info
 *   The info array.
 */

// Preprocess your variables here just like you would in a hook!
$variables['foo'] = 'bar';
```

The file discovery works exactly like template file discovery, which gives you
the flexibility to place your files in sub-folders and organize your projects
how you see fit. It also works with **suggestions**, allowing you to create
`node--article.preprocess.php`, for example.

This functionality works with **all** template hooks and suggestions, and can be
used in combination with
[Twig Debugging](https://www.drupal.org/docs/develop/theming-drupal/twig-in-drupal/debugging-twig-templates)
to create preprocessor files dedicated to their templates and properly segment
your code and perfect the organization of your projects.

You can then configure the module at the `/admin/config/ppf` page.

## Configuration

All relevant configuration for the module can be found at `/admin/config/ppf`.

Here, you can adjust the directory Preprocessor Files are loaded in, adjust the
file extension, or generate a starter folder in your default theme.

## How It Works

The inner-workings of this module are not too complex. It all starts in the
`ppf.module` file.

We implement a `hook_theme_registry_alter` hook to set up preprocessor files to be
executed during template rendering.

This works similarly to how Drupal Core handles preprocess functions.

For each preprocessor file created, the module will try to find a template hook
and if it's found, the preprocessor file is set to be executed to preprocess the variables for the given theme hook.
