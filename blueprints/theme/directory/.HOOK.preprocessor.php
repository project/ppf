<?php

/**
 * @file
 * Preprocess override for a template. Acts as a preprocess function.
 *
 * Any modifications done in this file are equivalent to doing them within a
 * hook_preprocess_HOOK() implementation.
 *
 * The modifications here execute after traditional hooks if both are
 * implemented.
 *
 * The loading of this file is provided by the Preprocessor Files module.
 * @see \Drupal\ppf\PreprocessorFiles
 *
 * Available variables:
 * @var array $variables
 *   The variables array. The same you would get from a traditional preprocess
 *   hook function.
 * @var string $hook
 *   The theme hook.
 * @var array $info
 *   The info array.
 */

// Preprocess your variables here just like you would in a hook!
$variables['foo'] = 'bar';
