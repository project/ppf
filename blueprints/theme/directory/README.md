# Preprocessor Files Directory

## Getting Started

Files in this directory are automatically loaded by the
[Preprocessor Files](https://www.drupal.org/project/ppf) module.

Your file should follow the same naming convention as the template you are
trying to preprocess.

e.g. To preprocess variables for `node.html.twig`, you would create a
`node.preprocess.php` file.

This functionality works for any template, including those with suggestions.

e.g. To preprocess variables for `node--article.html.twig`, you would create a
`node--article.preprocess.php` file.

## Configurations

This directory name as well as the file extensions can be configured at
`/admin/config/ppf`.

Happy preprocessing!
